# EzPlatform Admin URL Alias Redirects Bundle

## NB -- this seems to now be out-of-the-box functionality in Ibexa 4

This is an eZ Platform bundle to that listens for front-end url aliases while in an admin siteaccess, and redirects to the appropriate location in admin.

## Installation
Run `composer require`:

```bash
composer require contextualcode/ezplatform-admin-url-alias-redirects
```

Enable the bundle in `config/bundles.php`:

```php
    return [
        ...,
        ContextualCode\EzPlatformAdminURLAliasRedirectsBundle\EzPlatformAdminURLAliasRedirectsBundle::class => ['all' => true],
        ...,
    ];
``` 

## Usage

Usage is automatic once the bundle is enabled.

A short example:

If you have 3 siteaccesses:

1. `site_admin` domain `www.admin.com`
2. `site_1`, domain `www.site1.com`, root name `Site1`
    - Path `www.site1.com/Page1`
3. `site_2`, domain `www.site2.com`, root name `Site2`
    - Path `www.site2.com/Page2`

Out of the box, you can visit `Page1` in the admin interface at `www.admin.com/Site1/Page1`, and `Page2` at `www.admin.com/Site2/Page2`.

But what if you don't want to add the site root path, i.e. `/Site1` or `/Site2` each time you visit the page in admin?
This bundle solves it for you! You can visit `www.admin.com/Page1` and you'll be redirected to `www.admin.com/Site1/Page1`.
And `www.admin.com/Page2` will redirect you to `www.admin.com/Site2/Page2`, etc.

Objects with the same path except for the site root path will redirect to the first defined siteaccess that the user has access to.
